<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Character set configuration -->
    <meta charset="<?php bloginfo( 'charset' ); ?>" />

    <!-- Viewport configuration, scaling options -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Hide the browser UI -->
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!-- MS tile icons -->
    <meta name="msapplication-TileColor" content="#69f0ae">
    <meta name="msapplication-TileImage" content="favicons/mstile-144x144.png">

    <!-- Android toolbar color -->
    <meta name="theme-color" content="#69f0ae">

    <!-- Apple touch icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon-180x180.png">

    <!-- Android touch icons -->
    <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">

    <!-- Web app manifest file -->
    <link rel="manifest" href="favicons/manifest.json">

    <!-- Stylesheets -->
    <link href="<?php echo get_template_directory_uri() ,'/libs/font-awesome/css/font-awesome.min.css';?>" rel="stylesheet" media="all" type="text/css"/>
<!--    <link href="--><?php //echo get_template_directory_uri() ,'/libs/materialize/css/materialize.min.css';?><!--" rel="stylesheet" media="all" type="text/css"/>-->
    <!-- Site title -->
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>
<!--<body >-->