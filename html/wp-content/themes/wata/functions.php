<?php
if (!function_exists('EnqueueMyStyles')) {
function EnqueueMyStyles() {
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/libs/jquery/jquery-2.1.1.min.js');

    wp_enqueue_script( 'gmap', 'https://maps.googleapis.com/maps/api/js');

    wp_enqueue_style('my-custom-style', get_stylesheet_uri() , false);

    wp_enqueue_script( 'masonry', get_template_directory_uri() . '/libs/masonry/dist/masonry.pkgd.min.js');

    wp_enqueue_script( 'materialize-js', get_template_directory_uri() . '/libs/materialize/js/materialize.custom.min.js');

    wp_enqueue_style('materialize-css', get_template_directory_uri() . '/libs/materialize/css/materialize.min.css');

    wp_enqueue_script( 'config', get_template_directory_uri() . '/scripts/config.js');
    wp_enqueue_script( 'wata', get_template_directory_uri() . '/scripts/wata.js');

    wp_enqueue_script( 'build', get_template_directory_uri() . '/scripts/build.js');


    wp_enqueue_script( 'images-loaded', get_template_directory_uri() . '/libs/imagesloaded/imagesloaded.js');

    wp_enqueue_script( 'triangles', get_template_directory_uri() . '/libs/triangles/js/triangles.min.js');

    wp_enqueue_script( 'is-in-viewport', get_template_directory_uri() . '/libs/isinviewport/isInViewport.min.js');

//wp_enqueue_style('my-google-fonts', '//fonts.googleapis.com/css?family=PT+Serif+Caption:400,400italic', false, '20150320');

//wp_enqueue_style('my-main-style', get_stylesheet_uri(), false, '20150320');
}
}
add_action('wp_enqueue_scripts', 'EnqueueMyStyles');
?>


<!--wp_enqueue_style('my-custom-style', get_stylesheet_uri() . '/style.css', false, '20150320');-->
